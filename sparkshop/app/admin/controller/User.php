<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai  <876337011@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\admin\service\UserService;
use app\model\user\User as UserModel;
use app\model\user\UserLabel;
use think\facade\View;

class User extends Base
{
    /**
     * 获取列表
     */
    public function index()
    {
        if (request()->isAjax()) {

            $userService = new UserService();
            $list = $userService->getList(input('param.'))['data'];

            $userService = new UserService();
            $baseMap = $userService->buildBaseParam();

            $labelModel = new UserLabel();
            $labelList = $labelModel->getAllList([], 'id value,name', 'id asc')['data'];

            // 检测分销功能是否开启
            $hasInstallAgent = false;
            if (hasInstalled('agent')) {
                $hasInstallAgent = event('CheckAgentAppoint')[0]['data'];
            }

            return jsonReturn(0, 'success', [
                'list' => $list,
                'base' => $baseMap,
                'label' => $labelList,
                'agent' => $hasInstallAgent
            ]);
        }

        return View::fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $userService = new UserService();
            $res = $userService->addUser($param);
            return json($res);
        }
    }

    /**
     * 编辑
     */
    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $userService = new UserService();
            $res = $userService->editUser($param);
            return json($res);
        }

        $id = input('param.id');
        $userModel = new UserModel();
        View::assign([
            'info' => $userModel->findOne([
                'id' => $id
            ])['data']
        ]);

        return View::fetch();
    }

    /**
     * 余额编辑
     */
    public function balance()
    {
        $param = input('post.');

        $userService = new UserService();
        $res = $userService->changeBalance($param);
        return json($res);
    }
}
